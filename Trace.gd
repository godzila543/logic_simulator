extends Line2D

var sourceSocket = {} #{index: i, ref: chipRef, outputNumber: n}
var endSocket = {}

func setSource(index, ref, outputNumber):
	sourceSocket.index = index
	sourceSocket.ref = ref
	sourceSocket.outputNumber = outputNumber
	
func setEnd(index, ref, inputNumber):
	endSocket.index = index
	endSocket.ref = ref
	endSocket.inputNumber = inputNumber

func updatePoints():
	clear_points()
	add_point(sourceSocket.ref.getOutput(sourceSocket.outputNumber).rect_global_position - get_parent().rect_global_position+Vector2(12,12))
	add_point(endSocket.ref.getInput(endSocket.inputNumber).rect_global_position - get_parent().rect_global_position+Vector2(12,12))
	setColor(sourceSocket.ref.getOutput(sourceSocket.outputNumber).state)
	
func setColor(isOn):
	if isOn:
		set_modulate(Color(1,1,1))
	else:
		set_modulate(Color(0.1,0.1,0.1))
		
	
func _process(delta):
	pass
