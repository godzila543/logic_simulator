extends PanelContainer
var socketPreload = preload("res://Socket.tscn")
var beingDragged = false
# Called when the node enters the scene tree for the first time.
func initiate(chipID, mother):
	var chip = mother.chipBase[chipID]
	$Columns/Name.text = chipID
	for i in range(0, chip.numInputs):
		var newInput = socketPreload.instance()
		newInput.name = str(i)
		$Columns/Inputs.add_child(newInput)
	for i in range(0, chip.numOutputs):
		var newOutput = socketPreload.instance()
		newOutput.name = str(i)
		$Columns/Outputs.add_child(newOutput)
		
func eventHandling(event):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT:
			beingDragged = event.pressed
	if event is InputEventMouseMotion and beingDragged:
		rect_position += event.relative

func getInput(n):
	return $Columns/Inputs.get_node(str(n))

func getOutput(n):
	return $Columns/Outputs.get_node(str(n))
