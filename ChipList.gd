extends VBoxContainer


func chipClicked(event, chipID):
	if event.is_action_pressed("click_left"):
		var mother = get_owner()
		print(chipID)
		mother.get_node("Chip Editor").get_current_tab_control().addChip(chipID, mother)

func _ready():
	var mother = get_owner()
	mother.get_node("Side Panel/Chip List/NAND").connect("gui_input", mother.get_node("Side Panel/Chip List"), "chipClicked", ["NAND"])
