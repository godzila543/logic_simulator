extends GridContainer
var ICPreload = preload("res://IntegratedCircuit.tscn")

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Create_Chip_pressed():
	var mother = get_owner()
	var chipID = $"Name Input".text
	mother.createChip(chipID, $"Inputs Input".value, $"Outputs Input".value)
	mother.get_node("Chip Editor").createPanel($"Name Input".text)
	
	var newChip = ICPreload.instance() #create and add chip to the chip list
	newChip.initiate(chipID, mother)
	newChip.connect("gui_input", mother.get_node("Side Panel/Chip List"), "chipClicked", [chipID])
	mother.get_node("Side Panel").get_node("Chip List").add_child(newChip)
	pass # Replace with function body.


func _on_Inputs_Input_value_changed(value):
	$"Inputs Label".text = "Inputs: " + str(value)


func _on_Outputs_Input_value_changed(value):
	$"Outputs Label".text = "Outputs: " + str(value)


func _on_Start_Render_pressed():
	get_owner().get_node("Chip Editor").get_current_tab_control().shouldRender = true
