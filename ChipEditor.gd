extends TabContainer

var panelPreload = preload("res://ChipPanel.tscn")

func createPanel(chipID):
	var newPanel = panelPreload.instance()
	var mother = get_owner()
	newPanel.name = chipID
	newPanel.initiate(mother.chipBase[chipID].numInputs, mother.chipBase[chipID].numOutputs)
	add_child(newPanel)
	
func _ready():
	pass # Replace with function body.
