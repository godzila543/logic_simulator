extends HBoxContainer

var chipBase = {"NAND": 
					{"numInputs": 2, "numOutputs": 1} 
					
				} 
# {ID: {numInputs: x, numOutputs: y, innerChips: 
# 	{index: {ID: myChip, inputs: {[chipIndex, outputIndex],
#							      [chipIndex, outputIndex]
#}}}
func createChip(chipID, numInputs, numOutputs):
	chipBase[chipID] = {
		"numInputs": numInputs,
		"numOutputs": numOutputs,
		"innerChips": {}
	}

func updateChip(chipID, innerChips):
	chipBase[chipID].innerChips = innerChips

func createChipBody(chipID, initialState):
	var chipBody = {}
	for chipIndex in chipBase[chipID].innerChips:
		chipBody[chipIndex] = [] #table for containing outputs of each chip 	
	chipBody[0] = initialState
	return chipBody

func evaluate(chipID, initialState, returnBody):
	if initialState.size() != chipBase[chipID].numInputs: 
		#print("INCORRECT INPUT SIZE")
		while initialState.size() != chipBase[chipID].numInputs:
			initialState.append(0)
	if chipID == "NAND":
		return [not (initialState[0] and initialState[1])] #special case for nand because it is the base chip from which all is made
		
		
	var chipBody = createChipBody(chipID, initialState)
	var completeExecution = false
	for _stupidVar in range(0,50): 
		completeExecution = true
		for chipIndex in chipBody:
			var chip = chipBase[chipID].innerChips[chipIndex]
			if chipBody[chipIndex].size() > 0 and not chip.ID == "SELF": #check if chip has already been evaluated, skip if so
				continue
			
			var inputArray = []
			var completeInput = true
			for inputIndex in chip["inputs"]:
				var inputLocation = chip["inputs"][inputIndex]
				if chipBody[inputLocation[0]].size() == 0: #check if the node sending inputs has been evaluated
					completeExecution = false #if a chip cannot yet be evaluated, execution is not complete
					completeInput = false
					break #if any of the inputs are not yet declared do not evaluate
				var inputValue = chipBody[inputLocation[0]][inputLocation[1]]
				inputArray.append(inputValue)
			if !completeInput: continue
			#at this point, we know we are able to evaluate the current chip
			if chip.ID == "SELF":
				if returnBody:
					return chipBody
				else:
					return inputArray
			chipBody[chipIndex] = evaluate(chip.ID, inputArray, false)
		if completeExecution: break
				
func _init():
	pass
