extends TextureButton

var state = false
var editable = false

signal traceInfo(isInput, chipIndex, socketIndex)

func updateColor():
	if state:
		modulate = Color("f05454")
	else:
		modulate = Color("222831")

func setState(newState):
	if state!=newState:
		state = newState
		updateColor()

func _on_Circle_pressed():
	if editable: setState(!state)
	

func _ready():
	updateColor()
	if get_node("../..").get("isChipPanel"):
		print("OMEGALUL")
		connect("traceInfo", get_node("../.."), "receiveTraceSignal")
	else:
		connect("traceInfo", get_node("../../../../.."), "receiveTraceSignal")

func _on_Socket_gui_input(event):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and event.pressed:
			var isInput = get_parent().name == "Inputs"
			if get_node("../..").name == "Chip Panel":
				emit_signal("traceInfo", isInput, 0, int(name))
			else:
				emit_signal("traceInfo", isInput, int(get_node("../../..").name), int(name))
