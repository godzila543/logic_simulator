extends HBoxContainer

var socketPreload = preload("res://Socket.tscn")
var ICPreload = preload("res://IntegratedCircuit.tscn")
var tracePreload = preload("res://Trace.tscn")
var traceCreationInfo = {"originSet": false, "endSet": false}

var numChips = 0
var chips = {0: {"ID": "SELF", "ref": self, "inputs": {}}}
var traces = []
var mother
var shouldRender = false

var isChipPanel = true

func getInput(n):
	return $Inputs.get_node(str(n))

func getOutput(n):
	return $Outputs.get_node(str(n))


func initiate(numInputs, numOutputs):
	for i in range(0, numInputs):
		var newInput = socketPreload.instance()
		newInput.editable = true
		newInput.name = str(i)
		$Outputs.add_child(newInput)
	for i in range(0, numOutputs):
		var newOutput = socketPreload.instance()
		newOutput.name = str(i)
		$Inputs.add_child(newOutput)

func addChip(chipID, mother):
	numChips+=1
	var newChip = ICPreload.instance()
	newChip.initiate(chipID, mother)
	newChip.connect("gui_input", newChip, "eventHandling")
	newChip.name = str(numChips)
	$Workspace.add_child(newChip)
	chips[numChips] = {"ID": chipID, "ref": newChip, "inputs":{}}
	mother.updateChip(name, chips)
	
func receiveTraceSignal(isInput, chipIndex, socketIndex):
	if isInput:
		traceCreationInfo.endChipIndex = chipIndex
		traceCreationInfo.endSocketIndex = socketIndex
		traceCreationInfo.endSet = true
	else:
		traceCreationInfo.originChipIndex = chipIndex
		traceCreationInfo.originSocketIndex = socketIndex
		traceCreationInfo.originSet = true
	if traceCreationInfo.endSet and traceCreationInfo.originSet:
		createChipInput(traceCreationInfo.originChipIndex, traceCreationInfo.originSocketIndex, 
						traceCreationInfo.endChipIndex, traceCreationInfo.endSocketIndex)
		traceCreationInfo = {"originSet": false, "endSet": false}

func createTrace(originIndex, originN, endIndex, endN):
	var newTrace = tracePreload.instance()
	newTrace.setSource(originIndex, chips[originIndex].ref, originN)
	newTrace.setEnd(endIndex, chips[endIndex].ref, endN)
	$Workspace.add_child(newTrace)
	newTrace.updatePoints()
	traces.append(newTrace)
	
func createChipInput(originIndex, originN, endIndex, endN):
	chips[endIndex].inputs[endN]=[originIndex, originN]
	mother.updateChip(name, chips)
	
func generateTraces():
	for trace in traces:
		trace.free()
	traces = []
	for chipIndex in chips:
		for inputIndex in chips[chipIndex].inputs:
			var input = chips[chipIndex].inputs[inputIndex]
			createTrace(input[0], input[1], chipIndex, inputIndex)
			
func render():
	var inputTable = []
	for i in range(0, mother.chipBase[name].numInputs):
		inputTable.append($Outputs.get_node(str(i)).state)
	
	var chipBody = mother.evaluate(name, inputTable, true)
	for chipIndex in chips:
		for inputIndex in chips[chipIndex].inputs:
			var input = chips[chipIndex].inputs[inputIndex]
			var bodyState = chipBody[input[0]][input[1]]
			chips[input[0]].ref.getOutput(input[1]).setState(bodyState)
			chips[chipIndex].ref.getInput(inputIndex).setState(bodyState)
# Called when the node enters the scene tree for the first time.
func _ready():
	mother = get_tree().get_root().get_node("Motherboard")


func _process(delta):
	if shouldRender: render()
	generateTraces()
